#!/bin/bash
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

docker-compose -f docker-compose.yml down

docker-compose -f docker-compose.yml up -d ca.ninet.com orderer.ninet.com peer0.uitsec.ninet.com peer1.uitsec.ninet.com peer0.beebank.ninet.com peer1.beebank.ninet.com couchdb
docker ps -a

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=20
#echo ${FABRIC_START_TIMEOUT}
sleep ${FABRIC_START_TIMEOUT}

# Create the channel
docker exec -e "CORE_PEER_LOCALMSPID=UITSecMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@uitsec.ninet.com/msp" peer0.uitsec.ninet.com peer channel create -o orderer.ninet.com:7050 -c mychannel -f /etc/hyperledger/configtx/channel.tx
# Join peer0.uitsec.ninet.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=UITSecMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@uitsec.ninet.com/msp" peer0.uitsec.ninet.com peer channel join -b mychannel.block
