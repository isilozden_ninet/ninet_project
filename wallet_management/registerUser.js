/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { FileSystemWallet, Gateway, X509WalletMixin } = require('fabric-network');
const path = require('path');

const ccpPath = path.resolve(__dirname, 'ninet-network', 'connection-uitsec.json');

/**
 *  Create hakan user
 */
async function main() {
    try {

        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
        console.log(`Wallet path: ${walletPath}`);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('hakan');
        if (userExists) {
            console.log('An identity for the user "hakan" already exists in the wallet');
            return;
        }

        // Check to see if we've already enrolled the admin user.
        const adminExists = await wallet.exists('isil');
        if (!adminExists) {
            console.log('An identity for the admin user "isil" does not exist in the wallet');
            console.log('Run the enrollAdmin.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccpPath, { wallet, identity: 'isil', discovery: { enabled: true, asLocalhost: true } });

        // Get the CA client object from the gateway for interacting with the CA.
        const ca = gateway.getClient().getCertificateAuthority();
        const adminIdentity = gateway.getCurrentIdentity();

        // Add Affiliation for UITSEC Marketing
        //let affiliationService = ca.newAffiliationService();
        //let createAff = await affiliationService.create({ "name": "uitsec.marketing" }, adminIdentity);

        // Register the user, enroll the user, and import the new identity into the wallet.
        const secret = await ca.register({ affiliation: 'org1.department1', enrollmentID: 'hakan', role: 'client' }, adminIdentity);
        const enrollment = await ca.enroll({ enrollmentID: 'hakan', enrollmentSecret: secret });
        const userIdentity = X509WalletMixin.createIdentity('UITSecMSP', enrollment.certificate, enrollment.key.toBytes());
        await wallet.import('hakan', userIdentity);
        console.log('Successfully registered and enrolled admin user "hakan" and imported it into the wallet');

    } catch (error) {
        console.error(`Failed to register user "hakan": ${error}`);
        process.exit(1);
    }
}

main();
