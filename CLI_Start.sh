docker-compose -f docker-compose.yml up -d cli

echo "Channel Join Process Starting..."
docker cp peer0.uitsec.ninet.com:/opt/gopath/src/github.com/hyperledger/fabric/mychannel.block /tmp/mychannel.block
docker cp /tmp/mychannel.block cli:/opt/gopath/src/github.com/hyperledger/fabric/peer
echo "channel.block copied!"

# Join peer1.uitsec.ninet.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=UITSecMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/uitsec.ninet.com/users/Admin@uitsec.ninet.com/msp" -e "CORE_PEER_ADDRESS=peer1.uitsec.ninet.com:8051" cli peer channel join -b mychannel.block
echo "Peer1.UITSec Joined"

# Join peer0.beebank.ninet.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=BeeBankMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/beebank.ninet.com/users/Admin@beebank.ninet.com/msp" -e "CORE_PEER_ADDRESS=peer0.beebank.ninet.com:9051" cli peer channel join -b mychannel.block
echo "Peer0.BeeBank Joined"

# Join peer1.beebank.ninet.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=BeeBankMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/beebank.ninet.com/users/Admin@beebank.ninet.com/msp" -e "CORE_PEER_ADDRESS=peer1.beebank.ninet.com:10051" cli peer channel join -b mychannel.block
echo "Peer1.Beeank Joined!"

echo "Channel Joining Process Completed"
echo "CLI Container ENV. parameters rollback"
docker exec -e "CORE_PEER_LOCALMSPID=UITSecMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/uitsec.ninet.com/users/Admin@uitsec.ninet.com/msp" -e "CORE_PEER_ADDRESS=peer0.uitsec.ninet.com:7051" cli ls
