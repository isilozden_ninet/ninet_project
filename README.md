# NiNET_Project

Start Instructions:

* sh start.sh (Start HyperLedger Fabric Blockchain)
* sh CLI_Start.sh (Start Fabric CLI and Join all peers to default channel)
* sh peer_channel_list.sh (Show peers channels)
* sh chaincode_install.sh (Install the smart contract)
* sh chaincode_instantiate.sh (Instantiate the smart contract for all peers)
* sh monitordocker.sh (To show all HL instance status, transactions and block process)

Other Command Descriptions:


* CLI_getEnv.sh -> Show CLI environment variable values
* CLI_getOrgUserList.sh -> UITSec (Org1) user list
* chaincode_upgrade.sh -> to upgrade exist smart contract for HL fabric
* generate.sh -> Generate X.509 certificates, Public and Private keys for MSP (Membership Service Provider)
* init.sh -> Remove old keys and re-generate PEM files
* stop.sh -> Stop all containers with logspout monitoring tool
* teardown.sh -> Stop and remove all docker container also remove docker network
* showDockerContainerNameList.sh -> Show only docker container names
* showNiNET_Docker_Network.sh -> Shows docker container networks and details

Run Front-End Application To Manage API:

* npm start (execute this command when you in /uitsec/frontEndApp folder)


Get Smart Contract History From Ledger:

* node queryapp.js -> (It'll return NiNET commercial paper transaction history)

Wallet Management:

* node enrollAdmin.js -> (This command create admin for HL Fabric network by Fabric-CA)
* node registerUser.js -> (To register user for UITSec Org.)